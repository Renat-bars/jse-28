package ru.tsc.almukhametov.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import ru.tsc.almukhametov.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
