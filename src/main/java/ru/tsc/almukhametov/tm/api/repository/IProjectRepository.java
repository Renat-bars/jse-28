package ru.tsc.almukhametov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.model.Project;

import java.util.Optional;

public interface IProjectRepository extends IOwnerRepository<Project> {

    @NotNull
    Optional<Project> findByName(@NotNull String userId, @Nullable String name);

    @NotNull
    Optional<Project> removeByName(@NotNull String userId, @Nullable String name);

    @NotNull
    Project startById(@NotNull String userId, @Nullable String id);

    @NotNull
    Project startByIndex(@NotNull String userId, Integer index);

    @NotNull
    Project startByName(@NotNull String userId, @Nullable String name);

    @NotNull
    Project finishById(@NotNull String userId, @Nullable String id);

    @NotNull
    Project finishByIndex(@NotNull String userId, Integer index);

    @NotNull
    Project finishByName(@NotNull String userId, @Nullable String name);

    @NotNull
    Project changeProjectStatusById(@NotNull String userId, @Nullable String id, @NotNull Status status);

    @NotNull
    Project changeProjectStatusByIndex(@NotNull String userId, Integer index, @NotNull Status status);

    @NotNull
    Project changeProjectStatusByName(@NotNull String userId, @Nullable String name, @NotNull Status status);

}
