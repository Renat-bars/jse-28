package ru.tsc.almukhametov.tm.command.union;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.command.AbstractProjectTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectFindAllTasksCommand extends AbstractProjectTaskCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.TASK_LIST_BY_PROJECT;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.TASK_LIST_BY_PROJECT;
    }

    @Override
    public void execute() {
        @NotNull final String userId = String.valueOf(serviceLocator.getAuthenticationService().getCurrentUserId());
        System.out.println("[Task list by project]");
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final List<Task> tasks = serviceLocator.getProjectTaskService().findAllTaskByProjectId(userId, projectId);
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        System.out.println("Task list for project");
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task.toString());
            index++;
        }
    }

}
