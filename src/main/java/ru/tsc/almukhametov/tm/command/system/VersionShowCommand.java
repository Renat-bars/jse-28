package ru.tsc.almukhametov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.almukhametov.tm.command.AbstractCommand;
import ru.tsc.almukhametov.tm.constant.ArgumentConst;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;

public final class VersionShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.VERSION;
    }

    @NotNull
    @Override
    public String arg() {
        return ArgumentConst.VERSION;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.VERSION;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(serviceLocator.getPropertyService().getApplicationVersion());
    }

}
