package ru.tsc.almukhametov.tm.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public class Command {

    @Nullable
    private final String name;
    @Nullable
    private final String argument;
    @Nullable
    private final String description;

    public Command(@Nullable final String name, @Nullable final String argument, @Nullable final String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " ";
        if (argument != null && !argument.isEmpty()) result += " (" + argument + ") ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
