package ru.tsc.almukhametov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.almukhametov.tm.exception.AbstractException;

public class UnknownRoleException extends AbstractException {

    public UnknownRoleException(@NotNull final String role) {
        super("Error! Sort ``" + role + "`` was not found");
    }

}
